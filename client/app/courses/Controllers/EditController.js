angular.module('locations')
	.controller('CoursesEditCtrl', function ($scope, $stateParams) {
    	var model = store.courses.get($stateParams.courseId),
			modelData;

		modelData = model.toJSON();

		$scope.data = {
			id: modelData.id,
    		city: modelData.city,
    		groups: modelData.groups,
    		teachers: modelData.teachers
    	};

    	$scope.closeEditModal = function () {
    		model.save($scope.data);
		};
    });
