angular.module('locations')
	.controller('CoursesListCtrl', function ($scope) {
		$scope.courses = store.courses.toJSON();
		
		store.courses.on('all', function () {
		    $scope.courses = store.courses.toJSON();
		});

		$scope.deleteCourse = function (id) {
			var model = store.courses.get(id);

			store.courses.remove(model);
			model.destroy();
		}
	});
