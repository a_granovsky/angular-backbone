angular.module('locations')
	.config(['$stateProvider', '$urlRouterProvider',
	    function ($stateProvider, $urlRouterProvider) {
	    	var dir = 'app/courses/Views/';

	        $urlRouterProvider.otherwise('/');

	        $stateProvider
	            .state('courses', {
	                url: '/',
	                templateUrl: dir + 'courses.html'
	             })
	            
	            .state('courses.add', {
	                url: 'courses/add',
	                templateUrl: dir + 'create.html',
	                controller: 'CoursesCreateCtrl'
	             })

	            .state('courses.edit', {
	                url: 'courses/edit/:courseId',
	                templateUrl: dir + 'edit.html',
	                controller: 'CoursesEditCtrl'
	             });
	    }
]);