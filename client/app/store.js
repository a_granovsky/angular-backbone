'use strict';

function Store () {
    this.courses = new CoursesList();
    this.courses.fetch();
}

var store = new Store();