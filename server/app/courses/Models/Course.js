'use strict';
var Rotor = require('rotor-backbone');

var Course = Rotor.Model.extend({
    name: 'courses',

    defaults: {
		city: '',
		teachers: '',
		groups: ''
	}
});

module.exports = Course;